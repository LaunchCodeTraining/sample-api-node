const express = require("express");

const app = express();

app.use(express.json());

app.get("/", (req, res) => {
  const { query } = req;

  if (!Object.keys(query).length)
    return res.send("received simple GET request");

  return res.json(query);
});

app.post("/", (req, res) => {
  return res.json(req.body);
});

app.get("/headers", (req, res) => res.json(req.headers));

const { PORT = 8008 } = process.env;
app.listen(PORT, (err) => {
  if (err) return console.error(err);
  return console.log(`API listening on ${PORT}`);
});
