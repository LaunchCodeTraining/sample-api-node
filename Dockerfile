FROM node AS builder

COPY package*.json /

RUN npm install

FROM node

COPY --from=builder node_modules node_modules
COPY src/index.js .

CMD node .